# The COPYRIGHT file at the top level of this repository contains
# the full copyright notices and license terms.
import unittest
import datetime
from trytond.tests.test_tryton import ModuleTestCase, with_transaction
from trytond.tests.test_tryton import suite as test_suite
from trytond.transaction import Transaction
from trytond.pool import Pool
from trytond.exceptions import UserError
from trytond.ir.sequence import MissingError


class IrSequencePeriodTestCase(ModuleTestCase):
    """Test Ir Sequence Period module"""
    module = 'ir_sequence_period'

    @with_transaction()
    def test_incremental(self):
        'Test incremental'
        Sequence = Pool().get('ir.sequence')
        SequenceType = Pool().get('ir.sequence.type')
        SequencePeriod = Pool().get('ir.sequence.period')

        with Transaction().set_context(user=False, _check_access=False):
            with Transaction().set_user(0):
                type, = SequenceType.create([{
                    'name': 'Tests',
                }])
                sequence, = Sequence.create([{
                    'name': 'Test incremental',
                    'sequence_type': type.id,
                    'prefix': '${year}/',
                    'suffix': '',
                    'type': 'incremental',
                    }])
                period2, period1 = SequencePeriod.create([
                    {
                        'sequence': sequence.id,
                        'start_date': datetime.date(2018, 7, 1),
                        'end_date': datetime.date(2018, 12, 31),
                    }, {
                        'sequence': sequence.id,
                        'start_date': datetime.date(2018, 1, 1),
                        'end_date': datetime.date(2018, 6, 30),
                    }])
                # need date on context
                self.assertRaises(MissingError, sequence.get)

                self.assertEqual(period1.number_next, 1)
                self.assertEqual(period2.number_next, 1)
                with Transaction().set_context(
                        date=datetime.date(2018, 8, 15)):
                    self.assertEqual(sequence.get(), '2018/1')

                period1 = SequencePeriod(period1.id)
                period2 = SequencePeriod(period2.id)
                self.assertEqual(sequence.number_next, 1)
                self.assertEqual(period2.number_next, 2)
                self.assertEqual(period1.number_next, 1)

                Sequence.write([sequence], {
                        'number_increment': 10,
                        })
                with Transaction().set_context(
                        date=datetime.date(2018, 3, 15)):
                    self.assertEqual(sequence.get(), '2018/1')
                    self.assertEqual(sequence.get(), '2018/11')

                with Transaction().set_context(
                        date=datetime.date(2019, 1, 2)):
                    self.assertRaises(UserError, sequence.get)
                    period3, = SequencePeriod.create([{
                        'sequence': sequence.id,
                        'start_date': datetime.date(2019, 1, 1),
                        'end_date': datetime.date(2019, 12, 31),
                        'number_next': 40,
                        'suffix': '/XX'
                    }])
                    self.assertEqual(sequence.get(), '2019/40/XX')


def suite():
    suite = test_suite()
    suite.addTests(unittest.TestLoader().loadTestsFromTestCase(
            IrSequencePeriodTestCase))
    return suite
