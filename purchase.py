# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
from trytond.pool import PoolMeta
from trytond.pyson import Eval
from trytond.transaction import Transaction

__all__ = ['Purchase']


class Purchase(metaclass=PoolMeta):
    __name__ = 'purchase.purchase'

    @classmethod
    def __setup__(cls):
        super(Purchase, cls).__setup__()
        cls.purchase_date.states['required'] |= (Eval('state') == 'quotation')

    @classmethod
    def set_number(cls, purchases):
        for purchase in purchases:
            with Transaction().set_context(date=purchase.purchase_date):
                super(Purchase, cls).set_number([purchase])
