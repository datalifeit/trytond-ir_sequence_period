# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
from trytond.pool import PoolMeta, Pool
from trytond.transaction import Transaction

__all__ = ['ShipmentOut', 'ShipmentOutReturn', 'ShipmentIn',
    'ShipmentInReturn', 'ShipmentInternal']


class ShipmentMixin(object):

    @classmethod
    def create(cls, vlist):
        Date = Pool().get('ir.date')

        res = []
        for vals in vlist:
            with Transaction().set_context(date=vals.get('effective_date') or
                    vals.get('planned_date') or Date.today()):
                res.extend(super(ShipmentMixin, cls).create([vals]))
        return res


class ShipmentOut(ShipmentMixin, metaclass=PoolMeta):
    __name__ = 'stock.shipment.out'


class ShipmentOutReturn(ShipmentMixin, metaclass=PoolMeta):
    __name__ = 'stock.shipment.out.return'


class ShipmentIn(ShipmentMixin, metaclass=PoolMeta):
    __name__ = 'stock.shipment.in'


class ShipmentInReturn(ShipmentMixin, metaclass=PoolMeta):
    __name__ = 'stock.shipment.in.return'


class ShipmentInternal(ShipmentMixin, metaclass=PoolMeta):
    __name__ = 'stock.shipment.internal'
