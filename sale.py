# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
from trytond.pool import PoolMeta
from trytond.pyson import Eval
from trytond.transaction import Transaction

__all__ = ['Sale']


class Sale(metaclass=PoolMeta):
    __name__ = 'sale.sale'

    @classmethod
    def __setup__(cls):
        super(Sale, cls).__setup__()
        cls.sale_date.states['required'] |= (Eval('state') == 'quotation')

    @classmethod
    def set_number(cls, sales):
        for sale in sales:
            with Transaction().set_context(date=sale.sale_date):
                super(Sale, cls).set_number([sale])
